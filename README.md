NSI : jeux de rôle
==================

Ce dépôt Git contient un squelette, une coquille vide, au départ, qui permet de
programmer simplement des jeux de rôle, pour des projets dans le cadre
d'un enseignement NSI.

Voici les traits principaux de cette ressource :

Une utilisation du langage Python3, en priorité
-----------------------------------------------

Le langage Python est maintenant privilégié pour l'apprentissage de
l'informatique en lycée. Dans certains cas, comme quand on réalise une
application web, d'autres langages peuvent s'avérer nécessaires, comme
Javascript.

Depuis début 2020, Python est supporté en version 3. On peut l'utiliser avec
de nombreuses bibliothèques, qui facilitent la création de jeux de rôles :

  * **pygraphviz** (paquet python3-pygraphviz), qui permet de gérer
    facilement les graphes ; en particulier, on peut facilement définir
	autant d'attributs qu'on veut à chaque nœud.
  * **pydotplus** (paquet python3-pydotplus), qui fonctionne comme
    pygraphviz, mais est plus facile à manier quand on récupère les données
	depuis un fichier .dot
  * **cherrypy** (paquet python3-cherrypy3), qui permet de créer un site
    web avec quelques lignes de code
  * **jinja2** (paquet python3-jinja2), qui fournit un système puissant
    pour créer des modèles de pages web
  * **pyqt5** (paquets python3-pyqt5 et voisins), qui permet de développer
    des applications natives, à l'aide de l'interface graphique Qt
	
La coquille de départ utilise des composants aussi simples que possible
-----------------------------------------------------------------------

Il est important que seuls les concepts absolument nécessaires au développement
d'un jeu de rôles soient mobilisés.

Les outils mobilisés permettent beaucoup plus, mais c'est le rôle des élèves
qui créent des projets d'enrichir la construction initiale.

Un exemple fonctionnel, qui permet de se familiariser
-----------------------------------------------------

Dans le dossier exemple, on trouve un jeu de rôle avec quelques personnages
animaux, et un graphe de situations très limité.

L'exemple permet de se rendre compte de trois choses :

  * comment on crée le graphe des situations
  * comment on affiche le graphe des situations pour l'examiner à loisir
  * comment ce graphe des situations permet de faire fonctionner le jeu
    dans une interface utilisateur, web ou native
