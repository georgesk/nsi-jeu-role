#!/usr/bin/env python3
import cherrypy, os
import pydotplus as pdp
from jinja2 import Template

current_dir = os.path.dirname(os.path.abspath(__file__))

def getTemplate(fichier):
    """
    renvoie un objet jinja2.Template formé à partir du fichier
    qu'on trouve sous le répertoire "modeles" sous le nom fichier
    """
    page=""
    with open(os.path.join(current_dir, 'modeles', fichier)) as infile:
        page=infile.read()
    return Template(page)
        

def getAttribute(n, key, default=""):
    """
    récupère un attribut d'un nœud
    avec correction d'un bug dans la fonction get_attribute de la
    bibliothèque pydotplus

    @param n un nœud de graphe
    @param key la clé à rechercher
    @param default valeur à renvoyer si on ne trouve pas la clé
      (chaîne vide par défaut)
    @return la valeur de l'attribut
    """
    attributs = n.get_attributes()
    result = attributs.get(key, default)
    try:
        # si une chaîne est enclavée comme ceci : '"chaîne"'
        # elle sera désenclavée
        result1 = eval(result)
        result = result1
    except:
        pass
    return result

class JeuRole(object):

    def __init__(self, fichier_graphe):
        self.graph = pdp.graphviz.graph_from_dot_file(fichier_graphe)
        self.depart = self.graph.get_nodes()[0]
        self.courant = self.depart
        return
    
    @cherrypy.expose
    def index(self, nouveau=None):
        if nouveau:
            ## on recherche si un nœud possède ce nom-là
            new = self.graph.get_node(nouveau)
            if new:
                self.courant = new[0]
        n = self.courant
        liens = [
            l for l in self.graph.get_edges()
            if l.get_source() == n.get_name()
        ]
        choices={}
        for l in liens:
            c= getAttribute(l,"choice")
            dest = l.get_destination()
            choices[dest] = c
        return getTemplate('index.html').render(
            title="mon jeu de rôle",
            pagetitle = getAttribute(n,"title"),
            background = getAttribute(n,"background"),
            choices=choices
        )
    
conf = {
    '/images': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': os.path.join(current_dir, 'images'),
        'tools.staticdir.content_types': {'png': 'image/png'},
    },
        }
cherrypy.quickstart(JeuRole("mongraphe.dot"), "/", config = conf)
