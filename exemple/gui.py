#! /usr/bin/env python3
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton,\
   QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, QTimer
import pydotplus as pdp
from ui_jeu_natif import Ui_MainWindow

def getAttribute(n, key, default=""):
    """
    récupère un attribut d'un nœud
    avec correction d'un bug dans la fonction get_attribute de la
    bibliothèque pydotplus

    @param n un nœud de graphe
    @param key la clé à rechercher
    @param default valeur à renvoyer si on ne trouve pas la clé
      (chaîne vide par défaut)
    @return la valeur de l'attribut
    """
    attributs = n.get_attributes()
    result = attributs.get(key, default)
    try:
        # si une chaîne est enclavée comme ceci : '"chaîne"'
        # elle sera désenclavée
        result1 = eval(result)
        result = result1
    except:
        pass
    return result

class MonJeu(QMainWindow,Ui_MainWindow):
   def __init__(self, fichier_graphe, parent=None):
      QMainWindow.__init__(self, parent)
      self.graph = pdp.graphviz.graph_from_dot_file(fichier_graphe)
      self.depart = self.graph.get_nodes()[0]
      self.courant = self.depart
      self.setupUi(self)
      QTimer.singleShot(10, self.unTour)
      return

   def unTour(self):
      n = self.courant
      self.pageTitle.setText(getAttribute(n,"title"))
      style = """\
background-image: url({bg});
background-repeat: no-repeat;
""".format(bg=getAttribute(n,"background"))
      self.pageWidget.setStyleSheet(style);
      f=self.frame
      # s'assurer de la disposition verticale des boutons de choix
      if f.layout() is None:
         layout=QVBoxLayout()
         f.setLayout(layout)
      layout=f.layout()
      # déjà, supprimer tous les boutons de choix
      for c in f.findChildren(QPushButton):
         layout.removeWidget(c)
         del(c)
      # puis reconstruire les boutons de choix
      liens = [
         l for l in self.graph.get_edges()
         if l.get_source() == n.get_name()
      ]
      choices={}
      for l in liens:
         c= getAttribute(l,"choice")
         dest = l.get_destination()
         choices[dest] = c
      for dest, choix in choices.items():
         b = QPushButton(choix,f)
         style = "background: rgba(255,255,0,0.2); font-size: 24px;"
         b.setStyleSheet(style)
         b.clicked.connect(self.fabriqueTransition(dest))
         layout.addWidget(b)
         b.show()
      return

   def fabriqueTransition(self, dest):
      def transition():
         new = self.graph.get_node(dest)
         if new:
            self.courant = new[0]
         QTimer.singleShot(10, self.unTour)
         return
      return transition
   
def jeu(dotfile):
   app = QApplication(sys.argv)
   mw =  MonJeu(dotfile)
   mw.show()
   sys.exit(app.exec_())

if __name__ == '__main__':
   jeu("mongraphe.dot")
