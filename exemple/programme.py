#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = """Georges Khaznadar <georges.khaznadar@ac-lille.fr>"""

import pygraphviz as pgv

A=pgv.AGraph(      # création d'un graphe
    directed=True, # les liens sont directifs
)

### ajout des noeuds décorés par une image
A.add_node("canard",
           image= "images/canard-50.png",
           background= "images/canard.png",
           href= "https://fr.wikipedia.org/wiki/canard",
           title= "Ce canard est au début de tout")
A.add_node("ecureuil",
           image= "images/ecureuil-50.png",
           background= "images/ecureuil.png",
           href= "https://fr.wikipedia.org/wiki/écureuil",
           title="Bienvenue chez Picouic le hérisson")
A.add_node("herisson",
           image= "images/hamster-50.png",
           background= "images/hamster.png",
           href= "https://fr.wikipedia.org/wiki/hamster",
           title="C'est moi que tu regardes ?")
A.add_node("poussin",
           image= "images/poussin-50.png",
           background= "images/poussin.png",
           href= "https://fr.wikipedia.org/wiki/poussin",
           title="Mon œuf, ou-est-il déjà ?")
A.add_node("zebre",
           image= "images/zebre-50.png",
           background= "images/zebre.png",
           href= "https://fr.wikipedia.org/wiki/zèbre",
           title="le zèbre est la fin de tout")

### ajout des liens directifs
A.add_edge("canard", "ecureuil", len=1.5,
           choice="J'ai compris les règles du jeu, je passe à la suite")
A.add_edge("ecureuil", "herisson", len=1.5,
           choice="Je vais voir le hérisson")
A.add_edge("ecureuil", "poussin", len=1.5,
           choice="Je vais voir le poussin")
A.add_edge("poussin", "zebre", len=1.5,
           choice="Je vais voir le zèbre")
A.add_edge("herisson", "canard", len=1.5,
           choice="Désolé, retour au point de départ !")

print(A.string()) # affiche à l'écran

print("Écriture de mongraphe.dot")
A.write('mongraphe.dot') # enregistre un ficher au format .dot

A.layout() # mise en forme graphique par défaut (méthode neato)
A.draw('mongraphe.svg')
print("Écriture de mongraphe.svg")
